package tablaoperaciones;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class TablaOperaciones {

    public static void suma(int z, int y) throws IOException {
        InputStreamReader entrada = new InputStreamReader(System.in);
        BufferedReader flujoEntrada = new BufferedReader(entrada);

        String datoEntrada;
        int x, suma, i;


        System.out.print("Ingrese un numero(Suma): ");
        datoEntrada = flujoEntrada.readLine();
        x = Integer.parseInt(datoEntrada);
        for (i = 1; i <= 12; i++) {
            suma = x + i;
            System.out.print(x + " + " + i + " = " + suma + "\n");

        }
    }

    public static void resta(int z, int y) throws IOException {
        InputStreamReader entrada = new InputStreamReader(System.in);
        BufferedReader flujoEntrada = new BufferedReader(entrada);

        String datoEntrada;
        int x, resta, i;


        System.out.print("Ingrese un numero(Resta): ");
        datoEntrada = flujoEntrada.readLine();
        x = Integer.parseInt(datoEntrada);
        for (i = 1; i <= 12; i++) {
            resta = x - i;
            System.out.print(x + " - " + i + " = " + resta + "\n");
        }
    }

    public static void mult(int z, int y) throws IOException {
        InputStreamReader entrada = new InputStreamReader(System.in);
        BufferedReader flujoEntrada = new BufferedReader(entrada);

        String datoEntrada;
        int x, mult, i;


        System.out.print("Ingrese un numero(Multiplicación): ");
        datoEntrada = flujoEntrada.readLine();
        x = Integer.parseInt(datoEntrada);
        for (i = 1; i <= 12; i++) {
            mult = x * i;
            System.out.print(x + " * " + i + " = " + mult + "\n");
        }
    }

    public static void div(int z, int y) throws IOException {
        InputStreamReader entrada = new InputStreamReader(System.in);
        BufferedReader flujoEntrada = new BufferedReader(entrada);

        String datoEntrada;
        int i;
        float x, div;


        System.out.print("Ingrese un numero(División): ");
        datoEntrada = flujoEntrada.readLine();
        x = Float.parseFloat(datoEntrada);
        for (i = 1; i <= 12; i++) {
            div = (float) (x / i);
            System.out.print(x + " / " + i + " = " + div + "\n");
        }
    }

    public static void main(String[] args) throws IOException {
        int z = 0, y = 0;
        suma(z, y);
        resta(z, y);
        mult(z, y);
        div(z, y);





    }
}